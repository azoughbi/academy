= DESCRIPTION:

Configure APT to do unattended upgrades as security fixes are released.

= REQUIREMENTS:

Ubuntu or maybe Debian.

Tested on Ubuntu 10.04 LTS, 12.04 LTS, and 14.04 LTS.

= ATTRIBUTES:

The following node attributes are passed to the APT configuration template:

* unattended_upgrades[:upgrade_email] - email to receive notifications
* unattended_upgrades[:auto_reboot] - automatically reboot without confirmation if necessary (default false)

= USAGE:

  include_recipe "unattended_upgrades"

= CONTRIBUTING:

https://github.com/mcary/unattended_upgrades

== Testing:

  $ vagrant up $ver
  $ vagrant ssh $ver -- sudo sh /vagrant/test.sh

Where $ver is 10.04, 12.04, or 14.04.
